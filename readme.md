# Software Engineering Evaluation Test

Project assigned to me for my introduction to the company's code development process.

## Goal of this project

The goal was to create Java classes for the calculation of statistical metrics (minimum, maximum, mean, median and standard deviation) of arrays and array lists as well as a class that implements the algorithm Ascending Minima.
The classes that were created are the following:

* **StatisticUtilsArray**: This class calculates the statistical metrics of arrays containing double values.
The calculation of the statistical metrics is performed with the use of the library Apache Commons Math.
* **StatisticUtilsArrayList**: This class calculates the statistical metrics of array lists containing double values.
The calculation of the statistical metrics is performed with the use of the library Guava.
* **StatisticUtilsArrayStreams**:* This class calculates the statistical metrics of arrays containing double values.
The calculations are performed with the utilization of Java Streams.
* **StatisticUtilsArrayListStreams**: This class calculates the statistical metrics of array lists containing double values.
The calculations are performed  with the utilization of Java Streams.
* **StatisticUtilsArrayListGenerics**: This class calculates the statistical metrics of array lists containing any Numerical type of data.
The calculation of the statistical metrics is performed with the use of the library Guava.
* **AscendingMinima**: This class implements the algorithm Ascending Minima.
* **EfficientAscendingMinima**: This class implements the algorithm Ascending Minima following a more efficient approach by utilizing a Double Ending Queue (Deque).

In addition, the project includes .java files that implement the corresponding unit test classes. Each one of the test classes 
tests the methods of the corresponding Java class with normal and false inputs.

## Project dependencies 

The project was written in Java, using OpenJDK 15 and depends on the following libraries - plugins:

* Apache Commons Math - version 3.6.1
* JUnit Jupiter API - version 5.7.0
* JUnit Jupiter Engine - version 5.7.0
* JUnit Jupiter Params - version 5.7.0
* Maven Javadoc plugin - version 3.3.0
* Maven Project Info Reports plugin - version 3.1.2
* Maven Surefire plugin - version 2.22.2
* Guava - version 30.1.1-jre 