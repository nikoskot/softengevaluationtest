package com.company;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.util.Precision;

/**
 * Class that implements methods for the calculation of the statistical values of Min, Max, Median, Mean and
 * (population) Standard Deviation for arrays of doubles.
 */
public class StatisticUtilsArray {

    /**
     * Array of doubles that is used as input.
     */
    double[] array;

    /**
     * Statistics of the array.
     */
    DescriptiveStatistics stats;

    /**
     * Constructor for the class StatisticUtilsArray
     * @param arrayInput Array of doubles for statistical computations.
     */
    StatisticUtilsArray(double[] arrayInput){
        if (arrayInput == null) {                                       // Check if the input array is null. If it is throw an IllegalArgumentException.
            throw new IllegalArgumentException("Input array is null");
        }
        if (arrayInput.length == 0) {                                   // Check if th input array is empty. If it is throw an IllegalArgumentException.
            throw new IllegalArgumentException("Input array is empty");
        }
        this.array = arrayInput;
        this.stats = new DescriptiveStatistics(array);                  // The stats object of type DescriptiveStatistics will be used for the calculation of the statistic values.
    }

    /**
     * Method for getting th minimum value.
     * @return The minimum value of the array.
     */
    public double getMin(){
        return stats.getMin();
    }

    /**
     * Method for getting the maximum value.
     * @return The maximum value of the array.
     */
    public double getMax(){
        return stats.getMax();
    }

    /**
     * Method for getting the median.
     * @return The median of the array.
     */
    public double getMedian(){
        return Precision.round(stats.getPercentile(50), 2);
    }   // For simplicity the median is calculated with 2 decimal accuracy.

    /**
     * Method for getting the mean value with 2 decimal accuracy.
     * @return The mean value of the array.
     */
    public double getMean(){
        return Precision.round(stats.getMean(), 2); }      // For simplicity the mean is calculated with 2 decimal accuracy.

    /**
     * Method for getting the (population) standard deviation with 2 decimal accuracy.
     * @return The standard deviation of the array.
     */
    public double getStd() {
        StandardDeviation std = new StandardDeviation(false);
        return Precision.round(std.evaluate(this.array), 2);    // For simplicity the standard deviation is calculated with 2 decimal accuracy.
    }
}
