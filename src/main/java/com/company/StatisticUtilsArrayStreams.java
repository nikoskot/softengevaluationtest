package com.company;

import org.apache.commons.math3.util.Precision;
import java.util.DoubleSummaryStatistics;
import java.util.function.Supplier;
import java.util.stream.DoubleStream;

/**
 * Class that implements methods for the calculation of the statistical values of Min, Max, Median, Mean and
 * (population) Standard Deviation for arrays of doubles using java streams.
 */
public class StatisticUtilsArrayStreams {

    /**
     * Supplier for the repeated use of the stream.
     */
    Supplier<DoubleStream> streamSupplier;

    /**
     * Object for the stream statistics.
     */
    DoubleSummaryStatistics stats;

    /**
     * Constructor for the class StatisticUtilsArrayStreams
     * @param arrayInput Array of doubles for statistical computations.
     */
    StatisticUtilsArrayStreams(double[] arrayInput){
        if (arrayInput == null) {                                       // Check if the input array is null. If it is throw an IllegalArgumentException.
            throw new IllegalArgumentException("Input array is null");
        }
        if (arrayInput.length == 0) {                                   // Check if th input array is empty. If it is throw an IllegalArgumentException.
            throw new IllegalArgumentException("Input array is empty");
        }
        streamSupplier = () -> DoubleStream.of(arrayInput);             // Initialization of the DoubleStream supplier. Every time we call it, it returns th same stream which contains the elements of the input array.
        this.stats = this.streamSupplier.get().summaryStatistics();                  // The stats object of type DoubleSummaryStatistics will be used for the calculation of some statistical values.
    }                                                                                // SummaryStatistics contains getter functions for minimum, maximum, average and number of elements that will be used later.

    /**
     * Method for getting the minimum value.
     * @return The minimum value of the array.
     */
    public double getMin(){
        return this.stats.getMin();
    }

    /**
     * Method for getting the maximum value.
     * @return The maximum value of the array.
     */
    public double getMax(){
        return this.stats.getMax();
    }

    /**
     * Method for getting the median.
     * @return The median of the array.
     */
    public double getMedian(){
        long size = this.stats.getCount();  // Get the size of the stream.
        if (size % 2 != 0 ) {               // If the array contains odd number of elements
            return Precision.round(this.streamSupplier.get().sorted().toArray()[(int)size / 2], 2); // We sort the array and return the middle element.
        }
        else {      // If the array contains even number of elements.
            double[] sortedArray = this.streamSupplier.get().sorted().toArray();     // We sort the array and return the average of the two middle elements.
            return Precision.round(((sortedArray[(int)size / 2] + sortedArray[((int)size / 2) - 1]) / 2.0), 2);
        }   // For simplicity the median is calculated with 2 decimal accuracy.
    }

    /**
     * Method for getting the mean value with 2 decimal accuracy.
     * @return The mean value of the array.
     */
    public double getMean(){
        return Precision.round(this.stats.getAverage(), 2); }      // For simplicity the mean is calculated with 2 decimal accuracy.

    /**
     * Method for getting the (population) standard deviation with 2 decimal accuracy.
     * This function executes manual calculation of the standard deviation.
     * @return The standard deviation of the array.
     */
    public double getStd(){
        long size = this.stats.getCount();      // Get the number of the elements.
        if (size == 1) return 0;                // If the array contains only one element, the standard deviation is equal to 0.
        double mean = Precision.round(this.stats.getAverage(), 2);  // Get the mean of the elements.
        return Precision.round(Math.sqrt((this.streamSupplier.get().map((x) -> (x - mean) * (x - mean)).sum() / (size))), 2); // First we subtract the mean from all the elements.
        }   // Then raise the differences to the power of two and calculate the average of these values. // Finally, we calculate the square root of the average.
            // For simplicity the standard deviation is calculated with 2 decimal accuracy.
}
