package com.company;

import org.checkerframework.checker.units.qual.A;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;


/**
 * Class that implements the Ascending Minima algorithm based on the solution #2 provided here:
 * http://softwarelearner.blogspot.com/2011/04/minima-in-sliding-window.html
 */
public class AscendingMinima {

    /**
     * Input array of integers for the calculation of ascending minima.
     */
    int[] array;

    /**
     * Size of the sliding window.
     */
    int windowSize;

    /**
     * Constructor for the creation of an Ascending Minima type object. It is used for the initialization of the object's
     * variables.
     * @param arrayInput The input array for th calculation of Ascending Minima
     * @param windowInput The size of the sliding window.
     */
    AscendingMinima(int[] arrayInput, int windowInput){
        if (windowInput == 0 || arrayInput == null || arrayInput.length == 0){      // If the window has size equal to zero, or the array is null/empty, then throw an IllegalArgumentException type exception.
            throw new IllegalArgumentException("Wrong input argument");
        }
        if (windowInput > arrayInput.length){       // If the size of the window is greater than the size of the array, then throw an IllegalArgumentException type exception.
            throw new IllegalArgumentException("Window size must be smaller than or equal to array size");
        }
        this.array = arrayInput;
        this.windowSize = windowInput;
    }

    /**
     * Method for the execution of the ascending minima algorithm.
     * @return Array with the ascending minima values.
     */
    public int[] getAscendingMinima(){
        ArrayList<Integer> window = Arrays.stream(array).limit(windowSize).boxed().collect(Collectors.toCollection(ArrayList::new));    // From the given array extract the first window that has size equal to windowSize and save it as an ArrayList
        ArrayList<Integer> currentAscMinimaValues;      // This ArrayList will hold the current ascending minima values
        ReturnPair ascMinimaValuesAndWindowPair;    // ReturnPair objects hold two ArrayLists: 'ascMinimaValues' for the ascending minima values and 'window' for the next window.
        int[] results = new int[array.length - windowSize +1];  // This array will hold the final results

        currentAscMinimaValues = ascendingMinimaOfWindow(window);   // Calculate the ascending minima values of the starting window.
        for (int i=windowSize; i<array.length; i++){    // For the rest of the array
            results[i - windowSize] = currentAscMinimaValues.get(0);    // Get the current minimum value of the current ascending minimum values and insert it to the final results array
            ascMinimaValuesAndWindowPair = windowShift(window, currentAscMinimaValues, array[i]);   // we shift the window importing the elements one by one. For every new element we calculate the new window and ascending minima values.
            currentAscMinimaValues = ascMinimaValuesAndWindowPair.getFirstArrayList();
            window = ascMinimaValuesAndWindowPair.getSecondArrayList();
        }
        results[array.length - windowSize] = currentAscMinimaValues.get(0);
        return results;
    }

    /**
     * Method for the calculation of the ascending minima values of the first window.
     * @param window The window whose ascending minima values we want to calculate.
     * @return The ascending minima values of the first window in the form of an ArrayList.
     */
    public ArrayList<Integer> ascendingMinimaOfWindow(ArrayList<Integer> window){
        if (window == null) throw new IllegalArgumentException("Window is null.");
        if (window.size() == 0 || window.size() == 1) return window;    // If the window is empty or a single value, return it as it is.
        ArrayList<Integer> asm = new ArrayList<>();     // Ascending minima values.
        ArrayList<Integer> remains;     // Remaining elements of the window during the calculation.
        int minIndex = 0;           // Index of the window's minimum value.
        for (int i=1;i<window.size();i++){      //Find the minimum of the window.
            if (window.get(i)<=window.get(minIndex)) minIndex=i;
        }
        asm.add(window.get(minIndex));  //Add the minimum value to the ascending minima values list
        remains = window.stream().skip(minIndex+1).collect(Collectors.toCollection(ArrayList::new));    // The elements following the minimum value
        asm.addAll(ascendingMinimaOfWindow(remains));                                                   // will undergo the same procedure.
        return asm;
    }

    /**
     * Method that calculates the new ascending minima values of a window based on the previous ascending minima values,
     * the elements of the window and the new element that will be imported to the window. Implements the sliding
     * movement of the window.
     * @param previousWindow The elements of the window. At the start of the execution it holds the previous values. The
     *                       first element will be removed and the new element will be added at the end.
     * @param previousAscMinima Ascending minima values. At the start of the execution it holds the previous values,
     *                          but the new values are appended to the end.
     * @param newElement The new element that must be imported to the window.
     * @return An object of type ReturnPair with the new ascending minima values and the new window.
     */
    public ReturnPair windowShift(ArrayList<Integer> previousWindow, ArrayList<Integer> previousAscMinima, int newElement){
        if (previousWindow == null) throw new IllegalArgumentException("Previous window is null.");
        if (previousWindow.isEmpty()) throw new IllegalArgumentException("Previous window is empty.");
        if (previousAscMinima == null) throw new IllegalArgumentException("Previous ascending minima values list is null.");
        if (previousAscMinima.isEmpty()) throw new IllegalArgumentException("Previous ascending minima values list is empty..");
        previousWindow.add(newElement);     // We add the new elements at the end of the window arraylist/
        if (previousWindow.get(0).equals(previousAscMinima.get(0))){    // If the first element of the window (that we have to remove) is equal to the minimum ascending minima value
            previousAscMinima.remove(0);                          // we remove it from the ascending minima list as well.
        }
        previousWindow.remove(0);   // We remove the first element of the window array.
        while (previousAscMinima.get(previousAscMinima.size()-1) >= newElement){    // Before we add the new element to the end of the ascending minima list,
            previousAscMinima.remove(previousAscMinima.size()-1);             // we remove from the end the values that are greater or equal (until we find one that is not).
            if (previousAscMinima.size() == 0) break;
        }
        previousAscMinima.add(newElement);  // We add the new element to the end of the ascending minima values list.
        return new ReturnPair(previousAscMinima, previousWindow);
    }

    /**
     * This class is used to create objects that hold two ArrayLists.
     * These objects are used to return two ArrayLists from a method.
     */
    static class ReturnPair {

        ArrayList<Integer> firstArrayList;
        ArrayList<Integer> secondArrayList;

        /**
         * Constructor.
         * @param a The first ArrayList.
         * @param b The second ArrayList
         */
        ReturnPair(ArrayList<Integer> a, ArrayList<Integer> b){
            firstArrayList = a;
            secondArrayList = b;
        }

        /**
         * Getter method for the first ArrayList.
         * @return The first ArrayList.
         */
        ArrayList<Integer> getFirstArrayList(){ return firstArrayList;}

        /**
         * Getter method for the second ArrayList.
         * @return The second ArrayList.
         */
        ArrayList<Integer> getSecondArrayList(){ return secondArrayList;}
    }
}
