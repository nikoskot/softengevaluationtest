package com.company;

import com.google.common.math.*;
import org.apache.commons.math3.util.Precision;
import java.util.ArrayList;

/**
 * Class that implements methods for the calculation of the statistical values of Min, Max, Median, Mean and
 * (population) Standard Deviation for arrays lists of Numeric type.
 */
public class StatisticUtilsArrayListGenerics<N extends Number> {

    /**
     * Array list of Numeric type that is used as input.
     */
    ArrayList<N> arrayList;

    /**
     * Constructor for the class StatisticUtilsArrayListGenerics.
     * @param arrayListInput Array list of Numeric type for statistical computations.
     */
    StatisticUtilsArrayListGenerics(ArrayList<N> arrayListInput){
        if (arrayListInput == null) {                                       // Check if the input array list is null. If it is throw an IllegalArgumentException.
            throw new IllegalArgumentException("Input array list is null");
        }
        if (arrayListInput.isEmpty()){                                      // Check if the input array list is empty. If it is throw an IllegalArgumentException.
            throw new IllegalArgumentException("Input array list is empty");
        }
        this.arrayList = arrayListInput;
    }

    /**
     * Method for getting th minimum value.
     * @return The minimum value of the array list.
     */
    public double getMin(){
        return Stats.of(this.arrayList).min();
    }

    /**
     * Method for getting the maximum value.
     * @return The maximum value of the array list.
     */
    public double getMax(){
        return Stats.of(this.arrayList).max();
    }

    /**
     * Method for getting the median.
     * @return The median of the array list.
     */
    public double getMedian(){
        return Precision.round(Quantiles.median().compute(this.arrayList), 2);
    }// For simplicity the median is calculated with 2 decimal accuracy.

    /**
     * Method for getting the mean value with 2 decimal accuracy.
     * @return The mean value of the array list.
     */
    public double getMean(){
        return Precision.round(Stats.of(this.arrayList).mean(), 2); }   // For simplicity the mean is calculated with 2 decimal accuracy.

    /**
     * Method for getting the (population) standard deviation with 2 decimal accuracy.
     * @return The standard deviation of the array list.
     */
    public double getStd() {
        return Precision.round(Stats.of(this.arrayList).populationStandardDeviation(), 2);  // For simplicity the standard deviation is calculated with 2 decimal accuracy.
    }

}
