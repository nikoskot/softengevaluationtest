package com.company;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

/**
 * Class that implements the Ascending Minima algorithm efficiently using a Double Ended Queue - Deque
 */
public class EfficientAscendingMinima {

    /**
     * Input array of integers for the calculation of ascending minima.
     */
    int[] array;

    /**
     * Size of the sliding window.
     */
    int windowSize;

    /**
     * Constructor for the creation of an EfficientAscndingMinima type object. It is used for the initialization of the
     * object's variables.
     * @param arrayInput The input array for the calculation of Ascending Minima
     * @param windowInput The size of the sliding window.
     */
    EfficientAscendingMinima(int[] arrayInput, int windowInput){
        if (windowInput == 0 || arrayInput == null || arrayInput.length == 0){      // If the window has size equal to zero, or the array is null/empty, then throw an IllegalArgumentException type exception.
            throw new IllegalArgumentException("Wrong input argument");
        }
        if (windowInput > arrayInput.length){       // If the size of the window is greater than the size of the array, then throw an IllegalArgumentException type exception.
            throw new IllegalArgumentException("Window size must be smaller than or equal to array size");
        }
        this.array = arrayInput;
        this.windowSize = windowInput;
    }

    /**
     * Method for the execution of the ascending minima algorithm.
     * @return Array with the ascending minima values.
     */
    public int[] getEfficientAscendingMinima(){
        int[] results = new int[array.length - windowSize +1];  // The size of the resulting array is determined by the size of the input array and th size of the window.
        Deque<Integer> dq = new ArrayDeque<>();     // The deque holds the current ascending minima values list and the first element of the deque is the smallest of them.
        for (int i=0; i<array.length; i++){     // For every element of the array
            while (!dq.isEmpty() && dq.getLast()>=array[i]){    // remove from the end of the deque the elements that are greater than the element to be imported
                dq.removeLast();                                // until w find one that is not, or there are no more elements.
            }
            dq.addLast(array[i]);       // Add the element at the nd of the deque (current ascending minima values).
            if (i>=windowSize-1){         // When we process at least the first windowSize elements of the array, for each new element of the array
                results[i-windowSize+1] = dq.getFirst();  // save the smallest of the current ascending minima values to the results array.
                if (array[i-windowSize+1] == dq.getFirst()){
                    dq.removeFirst();       // If the fist element of the hypothetical window that is sliding (this element would get removed from the window)
                }                           // is equal to the smallest element of the ascending minima values, then remove it from the deque.
            }
        }
        return results;
    }
}
