package com.company;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class that implements some tests for the EfficientAscendingMinima class.
 */
class EfficientAscendingMinimaTest {

    /**
     * Method that creates the data set necessary for the normal input tests for the method getEfficientAscendingMinima().
     *
     * The data set is a 3D array. Specifically it is a 2D array whose 1st column holds the input arrays, the 2nd column
     * holds the size of the sliding window and the 3rd column the
     * array with the expected ascending minima values. Each row corresponds to a normal input test case.
     * @return 3D data set array.
     */
    public static int[][][] getEfficientAscendingMinimaNormalInput() {
        return new int[][][] {
                {{5,1,3,2,6,8,4,6},{3},{1,1,2,2,4,4}},
                {{6,2,1,5,3,4,8,7},{2},{2,1,1,3,3,4,7}},
                {{1,3,3,2,5,8,7,8,9,6},{3},{1,2,2,2,5,7,7,6}},
                {{1,3,3,2,5,8,7,8,9,10},{5},{1,2,2,2,5,7}},
                {{1,3,3,2,5,8,7,8,9,0},{4},{1,2,2,2,5,7,0}}
        };
    }

    /**
     * Method for testing the method getEfficientAscendingMinima() when given normal inputs.
     */
    @ParameterizedTest
    @MethodSource("getEfficientAscendingMinimaNormalInput")
    void getEfficientAscendingMinimaTest(int[][] data) {
        int[] arrayInput = data[0];
        int windowSize = data[1][0];
        int[] expected = data[2];
        EfficientAscendingMinima easm = new EfficientAscendingMinima(arrayInput, windowSize);
        assertArrayEquals(expected, easm.getEfficientAscendingMinima());
    }

    /**
     * Method for testing the function of the implemented EfficientAscendingMinima class when given false inputs.
     * This method makes sure that the constructor throws an exception when the input array is Null.
     */
    @Test
    void efficientAscendingMinimaNullArrayInputTest(){
        int[] arrayInput = null;
        int windowSize = 3;
        assertThrows(IllegalArgumentException.class, () -> new EfficientAscendingMinima(arrayInput, windowSize));
    }

    /**
     * Method for testing the function of the implemented EfficientAscendingMinima class when given false inputs.
     * This method makes sure that the constructor throws an exception when the input array is empty.
     */
    @Test
    void efficientAscendingMinimaEmptyArrayInputTest(){
        int[] arrayInput = {};
        int windowSize = 3;
        assertThrows(IllegalArgumentException.class, () -> new EfficientAscendingMinima(arrayInput, windowSize));
    }

    /**
     * Method for testing the function of the implemented EfficientAscendingMinima class when given false inputs.
     * This method makes sure that the constructor throws an exception when size of the window equals to zero.
     */
    @Test
    void efficientAscendingMinimaZeroWindowInputTest(){
        int[] arrayInput = {6,2,1,5,3,4,8,7};
        int windowSize = 0;
        assertThrows(IllegalArgumentException.class, () -> new EfficientAscendingMinima(arrayInput, windowSize));
    }

    /**
     * Method for testing the function of the implemented EfficientAscendingMinima class when given false inputs.
     * This method makes sure that the constructor throws an exception when the input array is Null and
     * the size of the window equals to zero.
     */
    @Test
    void efficientAscendingMinimaFalseInputsTest(){
        int[] arrayInput = null;
        int windowSize = 0;
        assertThrows(IllegalArgumentException.class, () -> new EfficientAscendingMinima(arrayInput, windowSize));
    }

    /**
     * Method for testing the function of the implemented EfficientAscendingMinima class when given false inputs.
     * This method makes sure that the constructor throws an exception when the size of the window is greater
     * than the size of the input array.
     */
    @Test
    void efficientAscendingMinimaLargerWindow(){
        int[] arrayInput = {1,2,3,4};
        int windowSize = 6;
        assertThrows(IllegalArgumentException.class, () -> new EfficientAscendingMinima(arrayInput, windowSize));
    }

}