package com.company;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class that implements some tests for the AscendingMinima class.
 */
class AscendingMinimaUnitTest {

    /**
     * Method that creates the data set necessary for the normal input tests for the method getAscendingMinima().
     *
     * The data set is a 3D array. Specifically it is a 2D array whose 1st column holds the input arrays, the 2nd column
     * holds the size of the sliding window and the 3rd column the
     * array with the expected ascending minima values. Each row corresponds to a normal input test case.
     * @return 3D data set array.
     */
    public static int[][][] getAscendingMinimaNormalInput() {
        return new int[][][] {
                {{5,1,3,2,6,8,4,6},{3},{1,1,2,2,4,4}},
                {{6,2,1,5,3,4,8,7},{2},{2,1,1,3,3,4,7}},
                {{1,3,3,2,5,8,7,8,9,6},{3},{1,2,2,2,5,7,7,6}},
                {{1,3,3,2,5,8,7,8,9,10},{5},{1,2,2,2,5,7}},
                {{1,3,3,2,5,8,7,8,9,0},{4},{1,2,2,2,5,7,0}}
        };
    }

    /**
     * Method that creates the data set necessary for the normal input tests for the method ascendingMinimaOfWindow().
     *
     * The data set is a 3D array. Specifically it is a 2D array whose 1st column holds the window that will be used as
     * input for the tests and the 2nd column holds the array with the expected ascending minima values. Each row
     * corresponds to a normal input test case.
     * @return 3D data set array.
     */
    public static int[][][] ascendingMinimaOfWindowNormalInput() {
        return new int[][][] {
                {{5,1,3,2},{1,2}},
                {{6,2,1,5,3,4,8},{1,3,4,8}},
                {{8,9,5,3,6,5,1,1,0},{0}},
                {{7,4,8,6,3,4,2,1,2},{1,2}},
                {{1,2,3,4,5,6,7,8,9},{1,2,3,4,5,6,7,8,9}},
                {{9,8,7,6,5,4,3,2,1},{1}},
                {{9,1,1,3,4,2,6,8,9},{1,2,6,8,9}},
                {{1},{1}},
                {{},{}}
        };
    }

    /**
     * Method that creates the data set necessary for the normal input tests for the method windowShift().
     *
     * The data set is a 3D array. Specifically it is a 2D array whose 1st column holds the window before the shift,
     * the 2nd column holds ascending minima before the shift, the 3rd column the new element that will be added to the
     * window, the 4th column the expected new ascending minima values after the shift and the 5th column the expected window after the
     * shift. Each row corresponds to a normal input test case.
     * @return 3D data set array.
     */
    public static int[][][] windowShiftNormalInput() {
        return new int[][][] {
                {{6,2,1,5,3,4,8},{1,3,4,8},{7},{1,3,4,7},{2,1,5,3,4,8,7}},
                {{1,3,3,2,5,8,7,8,9},{1,2,5,7,8,9},{6},{2,5,6},{3,3,2,5,8,7,8,9,6}},
                {{1,3,3,2,5,8,7,8,9},{1,2,5,7,8,9},{10},{2,5,7,8,9,10},{3,3,2,5,8,7,8,9,10}},
                {{1,3,3,2,5,8,7,8,9},{1,2,5,7,8,9},{0},{0},{3,3,2,5,8,7,8,9,0}}
        };
    }

    /**
     * Method for testing the method getAscendingMinima() when given normal inputs.
     */
    @ParameterizedTest
    @MethodSource("getAscendingMinimaNormalInput")
    void getAscendingMinimaTest(int[][] data) {
        int[] arrayInput = data[0];
        int windowSize = data[1][0];
        int[] expected = data[2];
        AscendingMinima asm = new AscendingMinima(arrayInput, windowSize);
        assertArrayEquals(expected, asm.getAscendingMinima());
    }

    /**
     * Method for testing the method ascendingMinimaOfWindow().
     */
    @ParameterizedTest
    @MethodSource("ascendingMinimaOfWindowNormalInput")
    void ascendingMinimaOfWindowTest(int[][] data) {
        int[] arrayInput = data[0];
        ArrayList<Integer> expected = Arrays.stream(data[1]).boxed().collect(Collectors.toCollection(ArrayList::new));  // The method returns an arrayList
        int[] arrayInputForConstructor = {1,2,3,4,5};   // These two variables take no specific part in the test. They are used only to create the object of type AscendingMinima
        int windowSizeForConstructor = 3;               //
        AscendingMinima asm = new AscendingMinima(arrayInputForConstructor, windowSizeForConstructor);
        assertEquals(expected, asm.ascendingMinimaOfWindow(Arrays.stream(arrayInput).boxed().collect(Collectors.toCollection(ArrayList::new))));
    }

    /**
     * Method for testing the method windowShift().
     */
    @ParameterizedTest
    @MethodSource("windowShiftNormalInput")
    void windowShiftTest(int[][] data) {
        ArrayList<Integer> previousWindow = Arrays.stream(data[0]).boxed().collect(Collectors.toCollection(ArrayList::new));    // windowShift() accepts the previous window as an arrayList
        ArrayList<Integer> previousAscMinima = Arrays.stream(data[1]).boxed().collect(Collectors.toCollection(ArrayList::new)); // windowShift() accepts the previous ascending minima values as an arrayList
        int newElement = data[2][0];
        ArrayList<Integer> expectedAscendingMinimaValues = Arrays.stream(data[3]).boxed().collect(Collectors.toCollection(ArrayList::new)); // windowShift returns two arrayLists
        ArrayList<Integer> expectedWindow = Arrays.stream(data[4]).boxed().collect(Collectors.toCollection(ArrayList::new));                //
        int[] arrayInputForConstructor = {1,2,3,4,5};     // These two variables take no specific part in the test. They are used only to create the object of type AscendingMinima
        int windowSizeForConstructor = 3;                 //
        AscendingMinima asm = new AscendingMinima(arrayInputForConstructor, windowSizeForConstructor);
        AscendingMinima.ReturnPair results = asm.windowShift(previousWindow, previousAscMinima, newElement);
        assertAll(
                () -> assertEquals(expectedAscendingMinimaValues,  results.firstArrayList),
                () -> assertEquals(expectedWindow, results.secondArrayList)
        );
    }

    /**
     * Method for testing the function of the implemented AscendingMinima class when given false inputs.
     * This method makes sure that the constructor throws an exception when the input array is Null.
     */
    @Test
    void ascendingMinimaNullArrayInputTest(){
        int[] arrayInput = null;
        int windowSize = 7;
        assertThrows(IllegalArgumentException.class, () -> new AscendingMinima(arrayInput, windowSize));
    }

    /**
     * Method for testing the function of the implemented AscendingMinima class when given false inputs.
     * This method makes sure that the constructor throws an exception when the input array is empty.
     */
    @Test
    void ascendingMinimaEmptyArrayInputTest(){
        int[] arrayInput = {};
        int windowSize = 7;
        assertThrows(IllegalArgumentException.class, () -> new AscendingMinima(arrayInput, windowSize));
    }

    /**
     * Method for testing the function of the implemented AscendingMinima class when given false inputs.
     * This method makes sure that the constructor throws an exception when size of the window equals to zero.
     */
    @Test
    void ascendingMinimaZeroWindowInputTest(){
        int[] arrayInput = {6,2,1,5,3,4,8,7};
        int windowSize = 0;
        assertThrows(IllegalArgumentException.class, () -> new AscendingMinima(arrayInput, windowSize));
    }

    /**
     * Method for testing the function of the implemented AscendingMinima class when given false inputs.
     * This method makes sure that the constructor throws an exception when the input array is Null and
     * the size of the window equals to zero.
     */
    @Test
    void ascendingMinimaFalseInputsTest(){
        int[] arrayInput = null;
        int windowSize = 0;
        assertThrows(IllegalArgumentException.class, () -> new AscendingMinima(arrayInput, windowSize));
    }

    /**
     * Method for testing the function of the implemented AscendingMinima class when given false inputs.
     * This method makes sure that the constructor throws an exception when the size of the window is greater
     * than the size of the input array.
     */
    @Test
    void ascendingMinimaLargerWindow(){
        int[] arrayInput = {1,2,3,4};
        int windowSize = 6;
        assertThrows(IllegalArgumentException.class, () -> new AscendingMinima(arrayInput, windowSize));
    }

    /**
     * Method for testing the function of the implemented AscendingMinima class when given false inputs.
     * This method makes sure that the method ascendingMinimaOfWindow throws an exception when the window is null.
     */
    @Test
    void ascendingMinimaOfWindowNullWindowTest(){
        ArrayList<Integer> window = null;
        int[] arrayInput = {1,2,3,4,5};
        int windowSize = 3;
        AscendingMinima asm = new AscendingMinima(arrayInput, windowSize);
        assertThrows(IllegalArgumentException.class, () -> asm.ascendingMinimaOfWindow(window));
    }

    /**
     * Method for testing the function of the implemented AscendingMinima class when given false inputs.
     * This method makes sure that the method windowShift throws an exception when the previous window is null.
     */
    @Test
    void windowShiftNullWindowTest(){
        ArrayList<Integer> previousWindow = null;
        ArrayList<Integer> previousAscMinima = new ArrayList<>();
        previousAscMinima.add(1);
        int newElement = 1;
        int[] arrayInput = {1,2,3,4,5}; // These two variables take no specific part in the test. They are used only to create the object of type AscendingMinima
        int windowSize = 3;             //
        AscendingMinima asm = new AscendingMinima(arrayInput, windowSize);
        assertThrows(IllegalArgumentException.class, () -> asm.windowShift(previousWindow, previousAscMinima, newElement));
    }

    /**
     * Method for testing the function of the implemented AscendingMinima class when given false inputs.
     * This method makes sure that the method windowShift throws an exception when the previous ascending minima list
     * is null.
     */
    @Test
    void windowShiftNullAscendingMinimaTest(){
        ArrayList<Integer> previousWindow = new ArrayList<>();
        previousWindow.add(1);
        ArrayList<Integer> previousAscMinima = null;
        int newElement = 1;
        int[] arrayInput = {1,2,3,4,5};     // These two variables take no specific part in the test. They are used only to create the object of type AscendingMinima
        int windowSize = 3;                 //
        AscendingMinima asm = new AscendingMinima(arrayInput, windowSize);
        assertThrows(IllegalArgumentException.class, () -> asm.windowShift(previousWindow, previousAscMinima, newElement));
    }

    /**
     * Method for testing the function of the implemented AscendingMinima class when given false inputs.
     * This method makes sure that the method windowShift throws an exception when the previous window is empty.
     */
    @Test
    void windowShiftEmptyWindowTest(){
        ArrayList<Integer> previousWindow = new ArrayList<>();
        ArrayList<Integer> previousAscMinima = new ArrayList<>();
        previousAscMinima.add(1);
        int newElement = 1;
        int[] arrayInput = {1,2,3,4,5}; // These two variables take no specific part in the test. They are used only to create the object of type AscendingMinima
        int windowSize = 3;             //
        AscendingMinima asm = new AscendingMinima(arrayInput, windowSize);
        assertThrows(IllegalArgumentException.class, () -> asm.windowShift(previousWindow, previousAscMinima, newElement));
    }

    /**
     * Method for testing the function of the implemented AscendingMinima class when given false inputs.
     * This method makes sure that the method windowShift throws an exception when the previous ascending minima list
     * is empty.
     */
    @Test
    void windowShiftEmptyAscendingMinimaTest(){
        ArrayList<Integer> previousWindow = new ArrayList<>();
        previousWindow.add(1);
        ArrayList<Integer> previousAscMinima = new ArrayList<>();
        int newElement = 1;
        int[] arrayInput = {1,2,3,4,5};     // These two variables take no specific part in the test. They are used only to create the object of type AscendingMinima
        int windowSize = 3;                 //
        AscendingMinima asm = new AscendingMinima(arrayInput, windowSize);
        assertThrows(IllegalArgumentException.class, () -> asm.windowShift(previousWindow, previousAscMinima, newElement));
    }

    /**
     * Method for testing the function of the implemented AscendingMinima class when given false inputs.
     * This method makes sure that the method windowShift throws an exception when all the inputs are null.
     */
    @Test
    void windowShiftNullInputsTest(){
        ArrayList<Integer> previousWindow = null;
        ArrayList<Integer> previousAscMinima = null;
        int newElement = 1;
        int[] arrayInput = {1,2,3,4,5};     // These two variables take no specific part in the test. They are used only to create the object of type AscendingMinima
        int windowSize = 3;                 //
        AscendingMinima asm = new AscendingMinima(arrayInput, windowSize);
        assertThrows(IllegalArgumentException.class, () -> asm.windowShift(previousWindow, previousAscMinima, newElement));
    }

    /**
     * Method for testing the function of the implemented AscendingMinima class when given false inputs.
     * This method makes sure that the method windowShift throws an exception when all the inputs are null.
     */
    @Test
    void windowShiftEmptyInputsTest(){
        ArrayList<Integer> previousWindow = new ArrayList<>();
        ArrayList<Integer> previousAscMinima = new ArrayList<>();
        int newElement = 1;
        int[] arrayInput = {1,2,3,4,5};     // These two variables take no specific part in the test. They are used only to create the object of type AscendingMinima
        int windowSize = 3;                 //
        AscendingMinima asm = new AscendingMinima(arrayInput, windowSize);
        assertThrows(IllegalArgumentException.class, () -> asm.windowShift(previousWindow, previousAscMinima, newElement));
    }

    /**
     * Method for testing the getFirstArrayList() method of ReturnPair class.
     */
    @Test
    void getFirstArrayListTest(){
        ArrayList<Integer> firstArrayList = new ArrayList<>();
        firstArrayList.add(1);
        ArrayList<Integer> secondArrayList = new ArrayList<>();
        AscendingMinima.ReturnPair result = new AscendingMinima.ReturnPair(firstArrayList, secondArrayList);
        assertEquals(firstArrayList, result.getFirstArrayList());
    }

    /**
     * Method for testing the getSecondArrayList() method of ReturnPair class.
     */
    @Test
    void getSecondArrayListTest(){
        ArrayList<Integer> firstArrayList = new ArrayList<>();
        ArrayList<Integer> secondArrayList = new ArrayList<>();
        secondArrayList.add(1);
        AscendingMinima.ReturnPair result = new AscendingMinima.ReturnPair(firstArrayList, secondArrayList);
        assertEquals(secondArrayList, result.getSecondArrayList());
    }
}