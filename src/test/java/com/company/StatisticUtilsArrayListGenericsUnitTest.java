package com.company;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class tests the methods of the class StatisticUtilsArrayListGenerics.
 */
class StatisticUtilsArrayListGenericsUnitTest <N extends Number>{

    /**
     * Method that creates the data set necessary for the normal input tests.
     *
     * The data set is a Stream of 6 arguments, each containing an array list of a specific Reference type to use as
     * input for the calculations (arrayList#) and an array of doubles with the expected results for minimum, maximum,
     * median, mean and standard deviation in that specific order (expected#).
     * @return Stream of input data.
     */
    public static Stream<Arguments> inputDataNormal(){
        Double[] temp1 = {22.0, 96.1, 50.0, 89.6, 48.4, 72.6, 50.0, 44.9, 82.3};
        ArrayList<Double> arrayList1 = Stream.of(temp1).collect(Collectors.toCollection(ArrayList::new));
        double[] expected1 = {22.0,96.1,50.0,61.77,23.13};

        Integer[] temp2 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        ArrayList<Integer> arrayList2 = Stream.of(temp2).collect(Collectors.toCollection(ArrayList::new));
        double[] expected2 = {1.0,9.0,5.0,5.0,2.58};

        Long[] temp3 = {1L, 1L, 1L, 1L, 1L, 1L, 1L, 1L, 1L};
        ArrayList<Long> arrayList3 = Stream.of(temp3).collect(Collectors.toCollection(ArrayList::new));
        double[] expected3 = {1.0,1.0,1.0,1.0,0.0};

        Short[] temp4 = {(short)0};
        ArrayList<Short> arrayList4 = Stream.of(temp4).collect(Collectors.toCollection(ArrayList::new));
        double[] expected4 = {0,0,0,0,0};

        Float[] temp5 = {-1.0F, 1.0F};
        ArrayList<Float> arrayList5 = Stream.of(temp5).collect(Collectors.toCollection(ArrayList::new));
        double[] expected5 = {-1,1,0,0,1};

        Byte[] temp6 = {(byte)29,(byte)33,(byte)63,(byte)8,(byte)22,(byte)88,(byte)27,(byte)57,(byte)27,(byte)72};
        ArrayList<Byte> arrayList6 = Stream.of(temp6).collect(Collectors.toCollection(ArrayList::new));
        double[] expected6 = {8,88,31,42.6,24.36};
        return Stream.of(
                Arguments.arguments(arrayList1, expected1),
                Arguments.arguments(arrayList2, expected2),
                Arguments.arguments(arrayList3, expected3),
                Arguments.arguments(arrayList4, expected4),
                Arguments.arguments(arrayList5, expected5),
                Arguments.arguments(arrayList6, expected6)
        );
    }

    /**
     * Method for testing the getMin() method for normal inputs.
     * @param arrayList The array list to use as input.
     * @param expected The expected results.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getMinNormalTest(ArrayList<N> arrayList, double[] expected) {
        StatisticUtilsArrayListGenerics<N> stats = new StatisticUtilsArrayListGenerics<>(arrayList);
        assertEquals(expected[0], stats.getMin());      // Use the "expected" array element that corresponds to the "minimum".
    }

    /**
     * Method for testing the getMax() method for normal inputs.
     * @param arrayList The array list to use as input.
     * @param expected The expected results.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getMaxNormalTest(ArrayList<N> arrayList, double[] expected) {
        StatisticUtilsArrayListGenerics<N> stats = new StatisticUtilsArrayListGenerics<>(arrayList);
        assertEquals(expected[1], stats.getMax(), 0.01);      // Use the "expected" array element that corresponds to the "maximum".
    }

    /**
     * Method for testing the getMedian() method for normal inputs.
     * @param arrayList The array list to use as input.
     * @param expected The expected results.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getMedianNormalTest(ArrayList<N> arrayList, double[] expected) {
        StatisticUtilsArrayListGenerics<N> stats = new StatisticUtilsArrayListGenerics<>(arrayList);
        assertEquals(expected[2], stats.getMedian());      // Use the "expected" array element that corresponds to the "median".
    }

    /**
     * Method for testing the getMean() method for normal inputs.
     * @param arrayList The array list to use as input.
     * @param expected The expected results.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getMeanNormalTest(ArrayList<N> arrayList, double[] expected) {
        StatisticUtilsArrayListGenerics<N> stats = new StatisticUtilsArrayListGenerics<>(arrayList);
        assertEquals(expected[3], stats.getMean());      // Use the "expected" array element that corresponds to the "mean".
    }

    /**
     * Method for testing the getStd() method for normal inputs.
     * @param arrayList The array list to use as input.
     * @param expected The expected results.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getStdNormalTest(ArrayList<N> arrayList, double[] expected) {
        StatisticUtilsArrayListGenerics<N> stats = new StatisticUtilsArrayListGenerics<>(arrayList);
        assertEquals(expected[4], stats.getStd());      // Use the "expected" array element that corresponds to the "standard deviation".
    }
    /**
     * Method that tests the constructor of StatisticUtilsArrayList class, given a null array as input.
     * Expects an exception of the type IllegalArgumentException.
     */
    @Test
    void nullInputTest(){
        ArrayList<N> arrayList = null;
        assertThrows(IllegalArgumentException.class, () ->
                new StatisticUtilsArrayListGenerics<>(arrayList));
    }

    /**
     * Method that tests the constructor of StatisticUtilsArrayList class, given an empty array as input.
     * Expects an exception of the type IllegalArgumentException.
     */
    @Test
    void emptyInputTest(){
        ArrayList<N> arrayList = new ArrayList<N>();
        assertThrows(IllegalArgumentException.class, () ->
                new StatisticUtilsArrayListGenerics<>(arrayList));
    }


}