package com.company;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;


import static org.junit.jupiter.api.Assertions.*;

/**
 * This class tests the methods of the class StatisticUtilsArray.
 */
class StatisticUtilsArrayUnitTest {

    /**
     * Method that creates the data set necessary for the normal input tests.
     *
     * The data set is a 3D array. Specifically it is a 2D array whose 1st column holds arrays with
     * the required inputs for the tests and the rest of the columns hold the expected values for the
     * minimum, the max, the median, the mean and standard deviation. Each row corresponds to a normal
     * input test case.
     * @return 3D data set array.
     */
    public static double[][][] inputDataNormal() {
            return new double[][][] {
                    {{22.0,96.1,50.0,89.6,48.4,72.6,50.0,44.9,82.3},{22.0},{96.1},{50.0},{61.77},{23.13}},
                    {{1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0},{1.0},{9.0},{5.0},{5.0},{2.58}},
                    {{1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0},{1.0},{1.0},{1.0},{1.0},{0}},
                    {{29.2,33.7,63.3,8.4,22.8,88.9,27.9,57.5,27.4,72.1},{8.4},{88.9},{31.45},{43.12},{24.35}},
                    {{0},{0},{0},{0},{0},{0}},
                    {{-1,1},{-1},{1},{0},{0},{1}}
            };
    }

    /**
     * Method for testing the getMin() method for normal inputs.
     * @param data 2D array that contains input data and expected min value.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getMinNormalTest(double[][] data) {
        double[] inputArray = data[0];
        double expected = data[1][0];       // From the expected values extract the on corresponding to "minimum".
        StatisticUtilsArray stats = new StatisticUtilsArray(inputArray);
        assertEquals(expected, stats.getMin());
    }

    /**
     * Method for testing the getMax() method for normal inputs.
     * @param data 2D array that contains input data and expected max value.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getMaxNormalTest(double[][] data) {
        double[] inputArray = data[0];
        double expected = data[2][0];       // From the expected values extract the on corresponding to "maximum".
        StatisticUtilsArray stats = new StatisticUtilsArray(inputArray);
        assertEquals(expected,stats.getMax());
    }

    /**
     * Method for testing the getMedian() method for normal inputs.
     * @param data 2D array that contains input data and expected median value.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getMedianNormalTest(double[][] data) {
        double[] inputArray = data[0];
        double expected = data[3][0];       // From the expected values extract the on corresponding to "median".
        StatisticUtilsArray stats = new StatisticUtilsArray(inputArray);
        assertEquals(expected,stats.getMedian());
    }

    /**
     * Method for testing the getMean() method for normal inputs.
     * @param data 2D array that contains input data and expected mean values.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getMeanNormalTest(double[][] data) {
        double[] inputArray = data[0];
        double expected = data[4][0];       // From the expected values extract the on corresponding to "mean".
        StatisticUtilsArray stats = new StatisticUtilsArray(inputArray);
        assertEquals(expected,stats.getMean());
    }

    /**
     * Method for testing the getStd() method for normal inputs.
     * @param data 2D array that contains input data and expected standard deviation values.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getStdNormalTest(double[][] data) {
        double[] inputArray = data[0];
        double expected = data[5][0];       // From the expected values extract the on corresponding to "standard deviation".
        StatisticUtilsArray stats = new StatisticUtilsArray(inputArray);
        assertEquals(expected,stats.getStd());
    }

    /**
     * Method that tests the constructor of StatisticUtilsArray class, given a null array as input.
     * Expects an exception of the type IllegalArgumentException.
     */
    @Test
    void nullInputTest(){
        double[] inputArray = null;
        assertThrows(IllegalArgumentException.class, () ->
                new StatisticUtilsArray(inputArray));
    }

    /**
     * Method that tests the constructor of StatisticUtilsArray class, given an empty array as input.
     * Expects an exception of the type IllegalArgumentException.
     */
    @Test
    void emptyInputTest(){
        double[] inputArray = {};
        assertThrows(IllegalArgumentException.class, () ->
                new StatisticUtilsArray(inputArray));
    }
}