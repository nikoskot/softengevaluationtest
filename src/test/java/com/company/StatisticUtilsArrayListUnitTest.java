package com.company;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class tests the methods of the class StatisticUtilsArrayList.
 */
class StatisticUtilsArrayListUnitTest {

    /**
     * Method that creates the data set necessary for the normal input tests.
     *
     * The data set is a Stream of 5 arguments, each containing an array list to use as input for the calculations
     * (arrayList#) and an array of doubles with the expected results for minimum, maximum, median, mean and standard deviation
     * in that specific order (expected#).
     * @return Stream of input data.
     */
    public static Stream<Arguments> inputDataNormal(){
        Double[] temp1 = {22.0, 96.1, 50.0, 89.6, 48.4, 72.6, 50.0, 44.9, 82.3};
        ArrayList<Double> arrayList1 = Stream.of(temp1).collect(Collectors.toCollection(ArrayList::new));
        double[] expected1 = {22.0,96.1,50.0,61.77,23.13};

        Double[] temp2 = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
        ArrayList<Double> arrayList2 = Stream.of(temp2).collect(Collectors.toCollection(ArrayList::new));
        double[] expected2 = {1.0,9.0,5.0,5.0,2.58};

        Double[] temp3 = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
        ArrayList<Double> arrayList3 = Stream.of(temp3).collect(Collectors.toCollection(ArrayList::new));
        double[] expected3 = {1.0,1.0,1.0,1.0,0.0};

        Double[] temp4 = {0.0};
        ArrayList<Double> arrayList4 = Stream.of(temp4).collect(Collectors.toCollection(ArrayList::new));
        double[] expected4 = {0,0,0,0,0};

        Double[] temp5 = {-1.0, 1.0};
        ArrayList<Double> arrayList5 = Stream.of(temp5).collect(Collectors.toCollection(ArrayList::new));
        double[] expected5 = {-1,1,0,0,1};

        Double[] temp6 = {29.2,33.7,63.3,8.4,22.8,88.9,27.9,57.5,27.4,72.1};
        ArrayList<Double> arrayList6 = Stream.of(temp6).collect(Collectors.toCollection(ArrayList::new));
        double[] expected6 = {8.4,88.9,31.45,43.12,24.35};
        return Stream.of(
                Arguments.arguments(arrayList1, expected1),
                Arguments.arguments(arrayList2, expected2),
                Arguments.arguments(arrayList3, expected3),
                Arguments.arguments(arrayList4, expected4),
                Arguments.arguments(arrayList5, expected5),
                Arguments.arguments(arrayList6, expected6)
        );
    }

    /**
     * Method for testing the getMin() method for normal inputs.
     * @param arrayList The array list to use as input.
     * @param expected The expected results.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getMinNormalTest(ArrayList<Double> arrayList, double[] expected) {
        StatisticUtilsArrayList stats = new StatisticUtilsArrayList(arrayList);
        assertEquals(expected[0], stats.getMin());      // Use the "expected" array element that corresponds to the "minimum".
    }

    /**
     * Method for testing the getMax() method for normal inputs.
     * @param arrayList The array list to use as input.
     * @param expected The expected results.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getMaxNormalTest(ArrayList<Double> arrayList, double[] expected) {
        StatisticUtilsArrayList stats = new StatisticUtilsArrayList(arrayList);
        assertEquals(expected[1], stats.getMax());      // Use the "expected" array element that corresponds to the "maximum".
    }

    /**
     * Method for testing the getMedian() method for normal inputs.
     * @param arrayList The array list to use as input.
     * @param expected The expected results.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getMedianNormalTest(ArrayList<Double> arrayList, double[] expected) {
        StatisticUtilsArrayList stats = new StatisticUtilsArrayList(arrayList);
        assertEquals(expected[2], stats.getMedian());      // Use the "expected" array element that corresponds to the "median".
    }

    /**
     * Method for testing the getMean() method for normal inputs.
     * @param arrayList The array list to use as input.
     * @param expected The expected results.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getMeanNormalTest(ArrayList<Double> arrayList, double[] expected) {
        StatisticUtilsArrayList stats = new StatisticUtilsArrayList(arrayList);
        assertEquals(expected[3], stats.getMean());      // Use the "expected" array element that corresponds to the "mean".
    }

    /**
     * Method for testing the getStd() method for normal inputs.
     * @param arrayList The array list to use as input.
     * @param expected The expected results.
     */
    @ParameterizedTest
    @MethodSource("inputDataNormal")
    void getStdNormalTest(ArrayList<Double> arrayList, double[] expected) {
        StatisticUtilsArrayList stats = new StatisticUtilsArrayList(arrayList);
        assertEquals(expected[4], stats.getStd());      // Use the "expected" array element that corresponds to the "standard deviation".
    }
    /**
     * Method that tests the constructor of StatisticUtilsArrayList class, given a null array as input.
     * Expects an exception of the type IllegalArgumentException.
     */
    @Test
    void nullInputTest(){
        ArrayList<Double> arrayList = null;
        assertThrows(IllegalArgumentException.class, () ->
                new StatisticUtilsArrayList(arrayList));
    }

    /**
     * Method that tests the constructor of StatisticUtilsArrayList class, given an empty array as input.
     * Expects an exception of the type IllegalArgumentException.
     */
    @Test
    void emptyInputTest(){
        ArrayList<Double> arrayList = new ArrayList<Double>();
        assertThrows(IllegalArgumentException.class, () ->
                new StatisticUtilsArrayList(arrayList));
    }
}